/* This is the implementation file for the queue, where you will define how the
 * abstract functionality (described in the header file interface) is going to
 * be accomplished in terms of explicit sequences of C++ statements. */

#include "queue.h"
#include <stdio.h>

/*Note that we will use the following "class invariants" for our data members:
1. frontIndx is the index of the element at the front of the queue, if any.
2. nextBackIndx is the index that is one after the back of the queue.
3. the elements data[frontIndx...nextBackIndx-1] are the valid elements
	of the queue, where each index in the range [frontIndx...nextBackIndx-1]
	is reduced modulo qArraySize ("modulo" === remainder === %-operator in C)
4. the empty queue will be represented by frontIndx == nextBackIndx
As a result of invariant 4, we must have that the queue is full when
nextBackIndx is one away from frontIndx: otherwise an insertion will make
the queue appear to be empty.  Note that "away from" in the preceding sentence
doesn't necessarily mean "one less" due to the circular way we use the array.*/

namespace csc212
{
	queue::queue()
	{
		/* initialize the queue to be empty. */
		this->nextBackIndx = 0;
		this->frontIndx = 0;
	}

	queue::~queue()
	{
		/* since we did not dynamically allocate any memory,
		 * there is nothing for us to release here.  */
	}

	unsigned long queue::getBack()
	{
		if (!(this->isEmpty()))
		{
			//number at index less than nextback
			return this->data[(((this->nextBackIndx) + qArraySize- 1)%qArraySize)];
		}

	}
	unsigned long queue::getFront()
	{
		return this->data[this->frontIndx];
	}
	unsigned long queue::getCapacity()
	{
		return csc212::qArraySize - 1;
	}

	unsigned long queue::getSize()
	{
		//check how many numbers from front to nextback
		return (((this->nextBackIndx+qArraySize)-(this->frontIndx))%qArraySize); 
	}

	bool queue::isEmpty()
	{
		return (this->nextBackIndx == this->frontIndx);

	}

	bool queue::isFull()
	{
		//check disance from nextback to front, full if 1
		if ((((this->frontIndx + qArraySize)-(this->nextBackIndx))%qArraySize)==1)
		{
			return true;
		}
		else {return false;} 
	}

	bool queue::push(unsigned long v)
	{
		if (!(this->isFull())) //check not full
		{
			//insert number, move nextback
			this->data[this->nextBackIndx]=v;
			this->nextBackIndx=(this->nextBackIndx+1)%qArraySize;
			return true;
		}
		else {return false;} 
	}

	unsigned long queue::pop()
	{
			unsigned long front;
		if (!(this->isEmpty()))
		{
			//get number in front, move front index forward
			front = this->data[this->frontIndx];
			this->frontIndx=((this->frontIndx+1)%qArraySize);
			return front;
		}
		return 0; 
	}

	void queue::erase()
	{
		this->nextBackIndx=this->frontIndx;
	}

	ostream& operator<<(ostream& o, const queue& q)
	{
		unsigned long i;
		for(i=q.frontIndx; i!=q.nextBackIndx; i=(i+1) % csc212::qArraySize)
			o << q.data[i] << "  ";
		return o;
	}
	void queue::print(FILE* f)
	{
		for(unsigned long i=this->frontIndx; i!=this->nextBackIndx;
				i=(i+1) % csc212::qArraySize)
			fprintf(f, "%lu ",this->data[i]);
		fprintf(f, "\n");
	}
}
