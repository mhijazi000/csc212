/* huffman encoding. */
#pragma once
#include <stddef.h> /* size_t */
#include <iostream>
using std::ostream;
#include <fstream>
using std::ofstream;
#include <algorithm>
using std::swap;
#include <functional>
using std::less;
#include <map>
using std::map;
#include <vector>
using std::vector;
#include <string>
using std::string;

/* forward declarations: */
template <typename T> struct hNode;
template <typename T>
void nodeToDot(hNode<T>* n, void* pFILE);

/* node for huffman encoding tree */
template <typename T>
struct hNode {
	T symbol;
	size_t weight;
	hNode<T>* left;
	hNode<T>* right;
	hNode() {left = right = 0;}
	typedef void (*nodeProcFn)(hNode<T>*,void*);
	void postOrderST(nodeProcFn f, void* pParams) {
		if(this == 0) return;
		left->postOrderST(f,pParams);
		right->postOrderST(f,pParams);
		f(this,pParams);
	}
	void drawDot(const char* fname) {
		ofstream fdot(fname);
		fdot << "digraph htree {\n" <<
				"  graph [ordering=\"out\"];\n" <<
				"  bgcolor=black\n" <<
				"  edge [color=white fontcolor=white]\n" <<
				"  node [style=filled color=white " <<
				"fillcolor=plum shape=circle]\n";
		this->postOrderST(&nodeToDot,(void*)&fdot);
		fdot << "}\n";
		fdot.close();
	}
};

/* a few utility functions for trees: */
template <typename T>
void nodeToDot(hNode<T>* n, void* pfdot)
{
	/* NOTE: the void* param is assumed to be a file stream which
	 * is opened for writing.  Also, you need to do this post order. */
	if (n==0) return;
	ofstream& fdot = *(ofstream*)(pfdot);
	/* print out leaf nodes with symbol and weight; print internal
	 * nodes with different color, and only weight. */
	if (!n->left && !n->right) {
		fdot << "  \"" << n << "\" [label=\"" << n->symbol <<
			" | " << n->weight <<"\" shape=box fillcolor=palegreen3]\n";
	} else {
		fdot << "  \"" << n << "\" [label=\"" << n->weight <<"\"]\n";
	}
	if (!(n->left||n->right)) return;
	/* for a Huffman tree, should be "full" (each node either
	 * has both children, or is a leaf) */
	if (n->left) {
		fdot << "  \""<<n<<"\" -> \""<<n->left<<"\" [label=0]\n";
	}
	if (n->right) {
		fdot << "  \""<<n<<"\" -> \""<<n->right<<"\" [label=1]\n";
	}
}

template <typename T>
void printLabels(hNode<T>* n, string& label)
{
	if (n==0) return;
	/* leaf node: print the symbol and label;
	 * non-leaf: append 0 for left, 1 for right. */
	if (!n->left && !n->right) {
		cout << "  " << n->symbol << "\t" << label << "\n";
		return;
	}
	label.push_back('0');
	printLabels(n->left,label);
	label.erase(label.size()-1);
	label.push_back('1');
	printLabels(n->right,label);
	label.erase(label.size()-1);
}

/*************** Heap functions ***************/
/* NOTE: most heap operations can be done in-place on an array.
 * Hence, we don't bother making a heap class, but instead just
 * provide a set of functions for arrays. */

/* utility macros for finding left / right and parent, assuming
 * 0-based indexing. */
#define LEFT(x)  ((x<<1)+1)
#define RIGHT(x) ((x+1)<<1)
#define PARENT(x) ((x-1)/2)

/* we want to compare pointers to hNodes by their weight: */
template <typename T>
struct hnComp {
	bool operator()(hNode<T>* n1, hNode<T>* n2) {return (n1->weight < n2->weight);}
};

/* heapify (downward). assumes that both left and right children of i
 * are max heaps (so that the only possible violation is at i). */
template <typename T, typename L>
void heapify(vector<T>& A, size_t i, L lt = less<T>())
{
	/* TODO: write this */
	size_t s = i;
	if(LEFT(i) < A.size() and lt(A[LEFT(i)],A[i])){
		s = LEFT(i);
	}
	if (RIGHT(i) < A.size() and lt(A[RIGHT(i)],A[s])){
		s = RIGHT(i);
	}
	if (s!=i) { 
		swap(A[i],A[s]);
		heapify(A,s,lt);
	}
}

/* we also need to heapify "upward" when inserting new elements: */
template <typename T, typename L>
void heapify_up(vector<T>& A, size_t i, L lt = less<T>())
{
	/* TODO: write this */
	if(i>0) 
	{
		if(lt(A[i],A[PARENT(i)])){
			swap(A[PARENT(i)],A[i]);
			heapify_up(A,PARENT(i),lt);
		}
	}
}

/* buildHeap.  Takes an un-ordered array and turns it into a heap. */
template <typename T, typename L>
void buildHeap(vector<T>& A, L lt = less<T>())
{
	/* Note that all leaves are heaps on their own.  So, you can work
	 * backwards through the tree calling heapify, starting from the
	 * last node's parent. */
	 size_t in = A.size()/2;
	 for (int i = in; i>=0; --i)
	 {
	 	heapify(A,i,lt);
	 }

	/* TODO: write this.  Make sure it runs in O(n) time. */
}

/************* priority queue functions. **************/
/* NOTE: we assume that every array passed to these functions is already a
 * heap!  NOTE: although the operations take only log(n) time, this is not
 * the most efficient priority queue implementation known -- see your book
 * for more advanced techniques which can reduce the *insert* time over a
 * sequence of calls.  Finally, note that the smallest element will always
 * be stored at index 0. */

/* remove and return front item from priority queue. */
template <typename T, typename L>
T pqPop(vector<T>& Q, L lt = less<T>())
{
	/* copy last value to top and reduce size (call pop_back). */
	/* TODO: write this */
	T t = Q[0]; // Had to get help for this one :s, got push right though ^^
	if(Q.size()>1) {
		swap(Q[0],Q[Q.size()-1]);
		Q.pop_back();
		buildHeap(Q,lt);
	}
	else Q.pop_back();
	return t;
}

/* insert new item into priority queue. */
template <typename T, typename L>
void pqPush(vector<T>& Q, T x, L lt = less<T>())
{
	/* push new item to back; then heapify up. */
	Q.push_back(x);
	heapify_up(Q, Q.size()-1,lt);
	/* TODO: write this */
}

/************* huffman encoding / decoding *************/

/* Take raw data and encode.  Return the root of the tree used
 * for the encoding.  (This would be needed if we ever wanted
 * to decode.)
 * NOTE: this function should allocate memory for nodes (as an array)
 * which the caller is then responsible for cleaning up.  See the
 * readme for more details. */
template <typename T>
hNode<T>* huffmanEncode(T* data, size_t len, hNode<T>*& nodes)
{
	/* TODO: write this */
	/* step 1: make a frequency table. */
    // count number of different data
    vector<T> unique;
    vector<int> counter;
    T tmp;
    size_t j;
    for (size_t i = 0; i < len; i++) {
        tmp = *(data + i);

	j = 0;
        for (; j < unique.size(); j++) {
            if (unique.at(j) == tmp) {
                counter[j] = counter[j] + 1;
                break;
            }
        }

        if (j >= unique.size()) {
            unique.push_back(tmp);
            counter.push_back(1);
        }

    }

	/* step 2: create array of nodes.  Note that 2n will suffice (this
	 * follows from the fact that a Huffman tree will always be full).
	 * step 3: initialize array from frequency table. */

    // create vector
    vector<hNode<T> * > A;
    nodes = new hNode<T> [unique.size()];
    for (size_t i = 0; i < unique.size(); i++) {
        nodes[i].weight = counter[i];
        nodes[i].symbol = unique[i];
        A.push_back(nodes + i );
    }

	/* since we don't want the nodes to ever be re-located (which would
	 * invalidate the pointers in the Huffman tree), we build our queue
	 * out of node pointers rather than the nodes themselves. */
	/* step 4: call buildHeap to create a priority queue; then build the
	 * tree by repeatedly "merging" the two nodes at the front of the
	 * queue, and then reinserting the merged node. */


    hnComp<T> comp;
    buildHeap(A, comp);
    hNode<T> * l = 0;
    hNode<T> * r = 0;

    while (A.empty() == false ) {

        l = pqPop(A, comp );

        if (A.empty()) {
            break;
        }
        r = pqPop(A, comp );
        hNode<T> * p = new hNode<T>();
        p->weight = l->weight + r->weight;
        p->left = l;
        p->right = r;
        pqPush(A, p, comp);
    }

	return l; /* just so it compiles. */
}
