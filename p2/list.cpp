/****************************************************
implementation file for linked list class.
*****************************************************/

#include "list.h"
#include <stdlib.h> //for rand()

namespace csc212
{
	/* listNode stuff: */
	listNode::listNode()
	{
		this->next = 0; //set the next pointer to be null
	}

	listNode::listNode(val_type x, listNode *pNext)
	{
		this->data = x;
		this->next = pNext;
	}


	/* linked list implementation: */
	list::list()
	{
		this->root = 0; //initialize the list to be empty
		//remember 0 == NULL, which is never memory you own.
	}

	list::list(const list &L)
	{
		/* TODO: write this. */
        this->root = 0;
        if ( L.root != 0 ) {
            listNode * curr = L.root;
            while (curr != 0) {
                insert(curr->data);
                curr = curr->next;
            }
        }
	}

	list::~list()
	{
		this->clear(); //delete all nodes, deallocating memory
	}

	list& list::operator=(const list& L)
	{
		/* TODO: write this. */
        if (this == &L) {
            return *this;
        }
        this->clear();
        root = 0;
        if ( L.root != 0 ) {
            listNode * curr = L.root;
            while (curr != 0) {
                insert(curr->data);
                curr = curr->next;
            }
        }
        return *this;
	}

	bool operator==(const list& L1, const list& L2)
	{

		/* TODO: write this. */
        listNode * left;
        listNode * right;

        left = L1.root;
        right = L2.root;

        while (left != 0
                && right != 0) {

            if (left->data != right->data) {
                return false;
            }
            left = left->next;
            right = right->next;
        }

        return left == 0 && right == 0 ;
            return false;  //just so it compiles.  you of course need to do something different.
	}
	bool operator !=(const list& L1, const list& L2)
	{
		return !(L1==L2);
	}

	void list::insert(val_type x)	
	{
		if(root == 0) {
			root = new listNode(x, 0);
		}
		else {
			listNode* prev = 0;
			listNode* curr = root;
			while(curr != 0) {
				if(curr->data > x) {
					break;
				}
				prev = curr;
				curr = curr->next;
			}
			if(prev == 0) {
				root = new listNode(x,curr);
			}
			else {
				listNode* t = new listNode(x,curr);
					prev->next = t;
			}
		}
	}

	void list::remove(val_type x)
	{
		/* TODO: write this. */
		if(root->data==x){
			listNode* temp = root;
			root = root->next;
			delete temp;
		}
		listNode* current;
	    listNode* prev;
	    prev = current = root;
	    while(current=current->next){
	        //check for match
	       if(current->data == x){
	           //let current listNode point to listNode after match
	           prev->next=current->next;
	           //release match from memory
	           delete current;
	           break;
	       }
	       //no match found, move to next listNode
	       prev=current;
	    }
	}

	bool list::isEmpty()
	{
		return (this->root == 0);
	}

	void list::clear()
	{
		//idea: repeatedly delete the root node...
		listNode* p;
		while((p = root)) //yes, I do mean "=", not "=="
		{
			root = p->next;
			delete p;
		}
	}

	ostream& operator<<(ostream& o, const list& L)
	{
		listNode* p;
		p = L.root;
		while(p)
		{
			o << p->data << " ";
			p = p->next;
		}
		return o;
	}

	bool list::search(val_type x) const
	{
		listNode* p;
		p = root;
		while(p && p->data != x) //again, short circuit evaluation is important...
			p = p->next;
		if(p)
			return true;
		else
			return false;
	}

	unsigned long list::length() const
	{
		/* TODO: write this. */
		unsigned long c=0;
		listNode* p;
		p = root;
			while(p){
				p = p->next;
				c++;
			}
		return c; //just so it compiles.  you of course need to do something different.
	}

	void list::merge(const list& L1, const list& L2)
	{
		/* TODO: write this. */
		listNode* nL1; 
		listNode* nL2; 
		listNode* newNode;
		nL1 = L1.root;
		nL2 = L2.root;
		if(nL1->data > nL2->data) 
			{
				newNode= new listNode(nL2->data);
				nL2=nL2->next;
			}
		else {
			newNode=new listNode(nL1->data); 
			nL1=nL1->next;
		}
		root=newNode;
		while(nL1&&nL2)
		{ //while both lists are not empty, compare the smallest of the two and add to this list
		    if ((nL1->data)<(nL2->data)){
		        newNode->next=new listNode(nL1->data);
		        newNode=newNode->next;
		        nL1=nL1->next;
		    }
		    else {
		        newNode->next=new listNode(nL2->data);
		        newNode=newNode->next;
		        nL2=nL2->next;
		    }
		}
		//whichever list is not yet empty, whatever is left is greater than everything above so just add to this list 
		while(nL1){
			newNode->next=new listNode(nL1->data);
		        newNode=newNode->next;
			nL1=nL1->next;
		}
		while(nL2){
			newNode->next=new listNode(nL2->data);
		        newNode=newNode->next;
			nL2=nL2->next;
		}
		//the main while loop runs in the number of times until one of the lists is empty. 
		//this takes no longer than L1+L2 time since atleast one list is advanced for each iteration let this be m.
		//only one of the while loops will run Order of number of elements left from the non-empty list.
		//the total run time will be the sum of the size of the two list, since first loop willl take O(m) where m<L1+l2.
		// and the second loop will take O(L1+L2-m)=> [(L1+L2-m)+m]=L1+L2
		
		//this algorithm should run in LINEAR TIME and set *this
		//to be the union of L1 and L2, and furthermore the list should remain sorted.
	}

	void list::randomFill(unsigned long n, unsigned long k)
	{
		//we want to fill the list with n random integers from 0..k-1
		this->clear(); //reset to the empty list
		unsigned long i;
		for(i=0; i<n; i++)
		{
			this->insert((val_type)(rand()%k));
		}
	}

	void list::intersection(const list &L1, const list& L2)
	{
		/* TODO: write this. */
		listNode* nL1; 
		listNode* nL2; 
		listNode* newNode;
		root = 0;
		nL1 = L1.root;
		nL2 = L2.root;
		while(nL1 && nL2){
			//check for same data
			if(nL1->data == nL2->data){
				//check if root node
				if(root){
				newNode->next = new listNode(nL1->data);
				newNode=newNode->next;}
				else{
					newNode = new listNode(nL1->data);
					root=newNode;
				}
				nL1= nL1->next;
				nL2=nL2->next;
			}
			//check for greater value and advance the other since no match can be found
			else if (nL1->data < nL2->data){
					nL1=nL1->next;
			}
			else { nL2= nL2->next;}
		}

		//this runs in the order of the sum of L1 and L2 because the while loop runs until
		//either L1 or L2 is exhausted, which will run out in linear time because for each iteration 
		//either one or both is advanced. therefore while runs for atleast length of either L1 or L2 or atmost L1+L2

		//this algorithm should run in LINEAR TIME, setting *this
		//to be the intersection (ordered) of L1 and L2.
	}
}
