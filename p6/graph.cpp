/* implementation of graph class. */

/* TODO: write this. */

#include <fstream>

#include "graph.h"

/**
 * constructor
 */
Graph::Graph(){
    nodes.clear();
    isDirected = true;
    // add predefined color to the graph
    colors.push_back("skyblue");
    colors.push_back("goldenrod");
    colors.push_back("greenyellow");
    colors.push_back("lightcyan");
    colors.push_back("orange");
    colors.push_back("saddlebrown");
    colors.push_back("lightskyblue");
    colors.push_back("palevioletred");
    colors.push_back("lightgray");
    colors.push_back("mintcream");
    colors.push_back("springgreen");
    colors.push_back("beige");
    colors.push_back("firebrick");
    colors.push_back("dimgray");
    colors.push_back("tomato");
     colors.push_back("violetred");


};
/**
 * destructor
 */
Graph::~Graph(){
    for (vector<Node *>::iterator it = nodes.begin() ;
            it != nodes.end(); ++it)
        delete *it;
};

/**
 * add new edge to graph
 * add source and destination node if it's needed
 * @param source
 * @param destination
 */
void Graph::addEdge(int source, int destination){
    Node * s = getNode(source);
    Node * d = getNode(destination);
    if (s == 0) { // add new node
        s = addNode(source);
        this->nodes.push_back(s);
    }
    if (d == 0) { // add new node
        d = addNode(destination);
        this->nodes.push_back(d);
    }
    // set up the edge
    s->adj.push_back(d);
}

/**
 * get a node from current graph by its name
 * @param name
 * @return 0 if there isn't any node to be found
 */
Node * Graph::getNode(int name){
    Node * node = 0;
    // iterate over the vector
    for (vector<Node *>::iterator it = nodes.begin() ;
            it != nodes.end(); ++it){
        if ((*it)->name == name) {
            node = (*it);
            break;
        }
    }
    return node;
}

/**
 * create and add new node to the graph
 * @param name
 * @return pointer to the new node
 */
Node * Graph::addNode(int name){
    Node * node;
    node = new Node;
    node->name = name;
    return node;
}

/**
 * reset several data item of the node
 */
void Graph::reset(){
     for (vector<Node *>::iterator it = nodes.begin() ;
            it != nodes.end(); ++it){
        (*it)->visisted = false;

        (*it)->distance = -1;
        (*it)->discovery = -1;
        (*it)->finish = -1;
    }
}

/**
 * start to process bfs algorithm from root node
 * @param root
 */
void Graph::bfs(Node * root){
    // create a new queue
    vector<Node *> queue;
    int distance = 0;
    // push root to queue
    queue.push_back(root);
    // init the root's distance
    root->distance = 0;
    // dequeue the queue until it becomes empty
    while (queue.empty() == false) {
        // get the head
        Node * current = queue.back();
        queue.pop_back();
        // this node has been visited
        current->visisted = true;
        // set the distance from current node to next considered node
        distance = current->distance + 1;

        // iterate over adjacency node of current node
        for (vector<Node *>::iterator it = current->adj.begin() ;
            it != current->adj.end(); ++it){
            if ((*it)->visisted == false) { // this node hasn't been visited
                (*it)->visisted = true;
                (*it)->distance = distance;
                (*it)->parent = current;

                // enqueue
                if (queue.empty()) { // this is for a simply usage  of queue
                    queue.push_back((*it));
                } else {
                    queue.insert(queue.begin(), (*it));
                }
            }
        }
    }
}

/**
 * find Node from the graph by its name
 * @param source
 */
void Graph::bfs(int source) {
  //  reset();
    //  reset related data item of all node
    for (vector<Node *>::iterator it = nodes.begin() ;
            it != nodes.end(); ++it){
        (*it)->visisted = false;
        (*it)->distance = -1;
        (*it)->parent = 0;
    }

    // find the right node then process it
    for (vector<Node *>::iterator it = nodes.begin() ;
            it != nodes.end(); ++it){
        if ((*it)->name == source) {
            bfs((*it));
            break;
        }
    }
}
